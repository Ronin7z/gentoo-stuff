# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DESCRIPTION=""
HOMEPAGE="https://github.com/Microsoft/vscode"
#SRC_URI="${HOMEPAGE}/archive/${PV}.tar.gz"
[ "$PV" = "9999" ] && EGIT_COMMIT="" || EGIT_COMMIT="${PV}"
EGIT_REPO_URI="${HOMEPAGE}.git"

inherit eutils git-r3

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 ~arm ~arm64 ~ppc ~ppc64 ~x86 ~amd64-linux ~x64-macos" # These are the architectures node supports, what about electron?
IUSE=""

RDEPEND="
	x11-libs/gtk+
	gnome-base/gconf
	x11-libs/libnotify
	x11-libs/libXScrnSaver
	x11-libs/libXtst
	x11-libs/libxkbfile
	dev-libs/nss
	media-libs/alsa-lib
"
DEPEND="
	net-libs/nodejs
	>=net-libs/nodejs-8.9.1 <=net-libs/nodejs-9.0.0
	${RDEPEND}
"

#S="{WORKDIR}/${PN}-${PV}"

src_prepare() {
	# This patch no longer contains proprietary modifications.
    # See https://github.com/Microsoft/vscode/issues/31168 for details.
	epatch "${FILESDIR}/product_json.patch"
    local _commit=$(git rev-parse HEAD)
    local _datestamp=$(date -u -Is | sed 's/\+00:00/Z/')
    sed -e "s/@COMMIT@/${_commit}/" -e "s/@DATE@/${_datestamp}/" \
        -i product.json

    # See https://github.com/MicrosoftDocs/live-share/issues/262 for details
    epatch "${FILESDIR}/code-liveshare.patch"

	eapply_user
}

src_compile() {
	# THIS IS VERRY BAD - I can't be bothered to deal with the whole npm versioning mess and package these individually though
	npm install gulp gulp-watch yarn
	export PATH="$PATH:$PWD/node_modules/.bin"

	_vscode_arch=x64

	case ${ABI} in
		amd64) _vscode_arch=x64;;
		arm|arm64) _vscode_arch="arm";;
		x86) _vscode_arch=ia32;;
		*) _vscode_arch="${ABI}";;
	esac

	yarn install --arch=$_vscode_arch

    # The default memory limit may be too low for current versions of node
    # to successfully build vscode.  Uncomment this to set it to 2GB, or
    # change it if this number still doesn't work for your system.
    mem_limit="--max_old_space_size=2048"

    if ! /usr/bin/env node $mem_limit "$(command -v gulp)" vscode-linux-${_vscode_arch}-min
    then
        echo
        echo "*** NOTE: If the build failed due to running out of file handles (EMFILE),"
        echo "*** you will need to raise your max open file limit."
        echo "*** This can be done by:"
        echo "*** 1) Set a higher 'nofile' limit (at least 10000) in either"
        echo "***    /etc/systemd/system.conf.d/limits.conf (for systemd systems)"
        echo "***    /etc/security/limits.conf (for non-systemd systems)"
        echo "*** 2) Reboot (or log out and back in)"
        echo "*** 3) Run 'ulimit -n' and ensure the value set above is shown before"
        echo "***    re-attempting to build this package."
        echo
        die
    fi

    cd "${WORKDIR}/VSCode-linux-${_vscode_arch}" || die

    # Patch the startup script to know where the app is installed, rather
    # than guessing...
    epatch "${FILESDIR}/startup_script.patch"
}

src_install() {
    install -m 0755 -d "${ED}/usr/share/code-oss"
    cp -r "${WORKDIR}/VSCode-linux-${_vscode_arch}"/* "${ED}/usr/share/code-oss"

    # Put the startup script in /usr/bin
    mv "${ED}/usr/share/code-oss/bin" "${ED}/usr"

	insinto /usr/share/applications
	doins "${FILESDIR}/${PN}.desktop"
}

# opencv

Updated ebuilds based on the ebuild posted in bug [#642800](https://bugs.gentoo.org/642800).

This builds fine with `FEATURES="test"` (not sure if that actually does anything to opencv?) but should not be considered stable. In case of issues I will likely revert this.

# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit git-r3

DESCRIPTION="An improved dynamic tiling window manager"
HOMEPAGE="https://github.com/Sweets/custard"
SRC_URI=""
EGIT_REPO_URI="${HOMEPAGE}.git"
EGIT_BRANCH="master"

LICENSE="MIT"
SLOT="0"
KEYWORDS=""
IUSE="doc examples"

CDEPEND="
	x11-libs/libxcb[xkb]
	x11-libs/xcb-util
	x11-libs/xcb-util-cursor
	x11-libs/xcb-util-keysyms
	x11-libs/xcb-util-wm
	x11-libs/xcb-util-xrm"
DEPEND="${CDEPEND}"
RDEPEND="${CDEPEND}"

src_compile() {
	emake
}

src_install() {
	emake DESTDIR="${D}" install
	einstalldocs
	if use examples
	then
		dodoc -r examples
	fi
}

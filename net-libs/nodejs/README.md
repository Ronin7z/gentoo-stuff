# NodeJS

Upstream only supports OpenSSL. There are patches to support LibreSSL for older versions but node is a fast-paced ecosystem so it's easier to just use the bundled openssl. This does that by removing the `--shared-openssl` from the ebuilds so node will fallback to using its bundled openssl.

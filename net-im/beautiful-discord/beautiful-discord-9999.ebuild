EAPI=6

PYTHON_COMPAT=( python3_{4,5,6} pypy{,3} )

inherit distutils-r1 git-r3

DESCRIPTION="Adds custom CSS support to Discord"
HOMEPAGE="https://github.com/leovoel/BeautifulDiscord"
SRC_URI=""
EGIT_REPO_URI="https://github.com/leovoel/BeautifulDiscord"
EGIT_BRANCH="master"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~alpha ~amd64 ~arm ~hppa ~ia64 ~ppc ~ppc64 ~s390 ~sh ~sparc ~x86"

RDEPEND="
		dev-python/psutil
"
DEPEND="${RDEPEND}
	dev-python/setuptools[${PYTHON_USEDEP}]
"

#S=${WORKDIR}/${P}-license

EAPI="5"

inherit font

DESCRIPTION="A programming font with ligatures"
HOMEPAGE="https://github.com/tonsky/FiraCode"
SRC_URI="https://github.com/ryanoasis/nerd-fonts/archive/v2.0.0.tar.gz"

LICENSE="mozilla"
SLOT="0"
KEYWORDS="~amd64"

S="${WORKDIR}"

FONT_S="${S}"
FONT_SUFFIX="ttf"

src_install() {
        insinto /usr/share/fonts/${PN}

        cd "${WORKDIR}/${PN}-${PV}/patched-fonts"

		find . -type f -name "*.ttf" -or -name "*.TTF" -or -name "*.otf" | while read -r line
		do
			[ -z "$line" ] && continue
			doins "$line"
		done

        font_xfont_config
        font_fontconfig
}

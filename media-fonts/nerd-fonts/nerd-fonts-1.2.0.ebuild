EAPI="5"

inherit font

DESCRIPTION="A programming font with ligatures"
HOMEPAGE="https://github.com/tonsky/FiraCode"
SRC_URI="
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/3270.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/AnonymousPro.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/Arimo.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/AurulentSansMono.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/BitstreamVeraSansMono.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/CodeNewRoman.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/Cousine.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/DejaVuSansMono.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/DroidSansMono.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/FantasqueSansMono.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/FiraCode.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/FiraMono.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/Go-Mono.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/Gohu.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/Hack.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/Hasklig.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/HeavyData.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/Hermit.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/Inconsolata.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/InconsolataGo.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/InconsolataLGC.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/Iosevka.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/Lekton.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/LiberationMono.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/Meslo.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/Monofur.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/Monoid.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/Mononoki.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/MPlus.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/ProFont.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/ProggyClean.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/RobotoMono.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/ShareTechMono.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/SourceCodePro.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/SpaceMono.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/Terminus.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/Tinos.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/Ubuntu.zip"
	"https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/UbuntuMono.zip"
"

LICENSE="mozilla"
SLOT="0"
KEYWORDS="~amd64"

S="${WORKDIR}"

FONT_S="${S}"
FONT_SUFFIX="ttf"

src_unpack() {
		#cp "${DISTDIR}"/*.ttf "${WORKDIR}"
		for f in "${DISTDIR}"/*.zip
		do
			7z x "$f" -o"${WORKDIR}"
		done
}

src_install() {
        insinto /usr/share/fonts/${PN}

        cd "${WORKDIR}"
        doins *.ttf

        font_xfont_config
        font_fontconfig
}

EAPI=6

#inherit git-r3

DESCRIPTION="Nerd fonts scripts"
HOMEPAGE="https://github.com/ryanoasis/nerd-fonts/tree/master/bin/scripts/lib"
SRC_URI="
	"https://raw.githubusercontent.com/ryanoasis/nerd-fonts/master/bin/scripts/lib/i_all.sh"
	"https://raw.githubusercontent.com/ryanoasis/nerd-fonts/master/bin/scripts/lib/i_dev.sh"
	"https://raw.githubusercontent.com/ryanoasis/nerd-fonts/master/bin/scripts/lib/i_fa.sh"
	"https://raw.githubusercontent.com/ryanoasis/nerd-fonts/master/bin/scripts/lib/i_fae.sh"
	"https://raw.githubusercontent.com/ryanoasis/nerd-fonts/master/bin/scripts/lib/i_iec.sh"
	"https://raw.githubusercontent.com/ryanoasis/nerd-fonts/master/bin/scripts/lib/i_linux.sh"
	"https://raw.githubusercontent.com/ryanoasis/nerd-fonts/master/bin/scripts/lib/i_oct.sh"
	"https://raw.githubusercontent.com/ryanoasis/nerd-fonts/master/bin/scripts/lib/i_ple.sh"
	"https://raw.githubusercontent.com/ryanoasis/nerd-fonts/master/bin/scripts/lib/i_pom.sh"
	"https://raw.githubusercontent.com/ryanoasis/nerd-fonts/master/bin/scripts/lib/i_seti.sh"
"


LICENSE="MIT"
SLOT="0"
KEYWORDS="~alpha ~amd64 ~arm ~hppa ~ia64 ~ppc ~ppc64 ~s390 ~sh ~sparc ~x86"

RDEPEND=""
DEPEND="${RDEPEND}"

S="${WORKDIR}"

src_install() {
  dodir '/usr/share/nerd-fonts'
  cp "${DISTDIR}/"*.sh "${ED}/usr/share/nerd-fonts"
}

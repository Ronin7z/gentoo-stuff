# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit user

DESCRIPTION="API Support for your favorite torrent trackers."
HOMEPAGE="https://github.com/Jackett/Jackett"
SRC_URI="${HOMEPAGE}/releases/download/v0.8.815/Jackett.Binaries.Mono.tar.gz"

LICENSE=""
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="dev-lang/mono"
RDEPEND="${DEPEND}"

S="${WORKDIR}/Jackett"

src_install() {
	cd ..
	mkdir -p "${ED}/var/lib"
	mv "Jackett" "${ED}/var/lib"

	newinitd "${FILESDIR}"/jackett.initd.10 jackett
}

pkg_postinst() {
	enewgroup jackett
	enewuser jackett -1 -1 /var/lib/Jackett jackett
	
	chown -R jackett:jackett "${EROOT%/}"/var/lib/Jackett || die
}

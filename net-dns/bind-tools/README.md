# bind-tools

A carbon-copy of the net-dns/bind-tools ebuild from the libressl overlay with fixes for [#651310](https://bugs.gentoo.org/651310)

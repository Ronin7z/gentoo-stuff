REPOMAN="repoman"

all: manifest tree

EXCLUDE=""
EBUILD=
EBUILD_ARGS=clean manifest merge
ebuild:
	$@ $(EBUILD) $(EBUILD_ARGS)

manifest:
	$(REPOMAN) $@

tree:
	sed -i '/^<!-- ::BEGIN_TREE:: -->$$/,$$ d' README.md
	printf '<!-- ::BEGIN_TREE:: -->\n' >> README.md
	tree -d -H 'https://gitlab.com/ahrs/gentoo-stuff/tree/master' -T '' --noreport -I 'files|profiles|metadata|eclass|$(EXCLUDE)' | pandoc -f html -t markdown_strict | tail +2 >> README.md


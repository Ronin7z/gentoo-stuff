EAPI=6

inherit git-r3 cmake-utils

DESCRIPTION="A Pulseaudio mixer in Qt (port of pavucontrol)"
HOMEPAGE="https://github.com/lxde/pavucontrol-qt"
EGIT_REPO_URI="https://github.com/lxde/pavucontrol-qt.git"
EGIT_BRANCH="master"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~alpha ~amd64 ~arm ~hppa ~ia64 ~ppc ~ppc64 ~s390 ~sh ~sparc ~x86"
IUSE="pulseaudio"

RDEPEND="
		media-sound/pulseaudio
"
DEPEND="
		${RDEPEND}
		dev-util/lxqt-build-tools
"

src_configure() {
	cmake-utils_src_configure \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
        -DCMAKE_BUILD_TYPE=Release
}

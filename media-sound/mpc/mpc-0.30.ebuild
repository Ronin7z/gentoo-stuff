# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6
inherit bash-completion-r1 meson

DESCRIPTION="A commandline client for Music Player Daemon (media-sound/mpd)"
HOMEPAGE="https://www.musicpd.org"
SRC_URI="https://www.musicpd.org/download/${PN}/${PV%.*}/${P}.tar.xz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="amd64 arm hppa ppc ppc64 sparc x86"
IUSE="iconv"

RDEPEND=">=media-libs/libmpdclient-2.9
	iconv? ( virtual/libiconv )
	doc? ( dev-python/sphinx )"
DEPEND="${RDEPEND}
	virtual/pkgconfig"

IUSE="iconv doc"

DOCS=( AUTHORS NEWS README.rst contrib/mpd-m3u-handler.sh contrib/mpd-pls-handler.sh )

src_configure() {
	local emesonargs=(
		$(meson_use iconv)
	)
	meson_src_configure
}

src_install() {
	default
	meson_src_install
	mv "${ED}/usr/share/doc/mpc" "${ED}/usr/share/doc/mpc-$PF"
	newbashcomp contrib/mpc-completion.bash ${PN}
}

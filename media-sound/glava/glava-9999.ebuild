EAPI=6

PYTHON_COMPAT=( python3_{4,5,6} pypy{,3} )

inherit git-r3

DESCRIPTION="cli and curses mixer for pulseaudio"
HOMEPAGE="https://github.com/wacossusca34/glava"
SRC_URI=""
EGIT_REPO_URI="${HOMEPAGE}.git"
EGIT_BRANCH="master"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~alpha ~amd64 ~arm ~hppa ~ia64 ~ppc ~ppc64 ~s390 ~sh ~sparc ~x86"
IUSE="pulseaudio opengl"

RDEPEND="
		media-sound/pulseaudio
		media-libs/glfw
"
DEPEND="${RDEPEND}
"

src_compile() {
	#git checkout 79d06b070e358dc29cff14435cd497248af10470
	make DISABLE_GLX=1 || die
}

src_install() {
	unset XDG_CONFIG_DIRS
	make DESTDIR="${ED}" install
}

#S=${WORKDIR}/${P}-license

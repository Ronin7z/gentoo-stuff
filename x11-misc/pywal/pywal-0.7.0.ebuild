EAPI=6

PYTHON_COMPAT=( python3_{5,6} pypy{,3} )

inherit distutils-r1

DESCRIPTION="🎨 Generate and change colorschemes on the fly. A 'wal' rewrite in Python 3."
HOMEPAGE="https://github.com/dylanaraps/pywal"
SRC_URI="${HOMEPAGE}/archive/${PV}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~alpha ~amd64 ~arm ~hppa ~ia64 ~ppc ~ppc64 ~s390 ~sh ~sparc ~x86"
IUSE=""

RDEPEND="
		media-gfx/imagemagick
"
DEPEND="${RDEPEND}
	dev-python/setuptools[${PYTHON_USEDEP}]
"

# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6
inherit git-r3 autotools toolchain-funcs

DESCRIPTION="interactive process viewer"
HOMEPAGE="https://gitcrate.org/qtools/rofi-top"
SRC_URI=""
EGIT_REPO_URI="${HOMEPAGE}.git"
EGIT_BRANCH="master"


LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"

RDEPEND="
	x11-misc/rofi
	gnome-base/libgtop
"
DEPEND="
	${RDEPEND}
"

src_prepare() {
	default

	eautoreconf
}

src_configure() {
	tc-export CC

	econf
}

src_compile() {
	# Compile defaults to clang which produced a crash for me
	# possibly unrelated.
	emake CC=gcc
}

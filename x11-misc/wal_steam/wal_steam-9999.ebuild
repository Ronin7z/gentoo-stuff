EAPI=6

PYTHON_COMPAT=( python3_{5,6} pypy{,3} )

inherit distutils-r1 git-r3

DESCRIPTION="A little script that themes the colours for Metro for steam from wal or wpg."
HOMEPAGE="https://github.com/kotajacob/wal_steam"
SRC_URI=""
EGIT_REPO_URI="$HOMEPAGE"
EGIT_BRANCH="master"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~alpha ~amd64 ~arm ~hppa ~ia64 ~ppc ~ppc64 ~s390 ~sh ~sparc ~x86"
IUSE=""

RDEPEND="
		media-gfx/imagemagick
		games-util/steam-meta
"
DEPEND="${RDEPEND}
	dev-python/setuptools[${PYTHON_USEDEP}]
"

EAPI=6

inherit git-r3

DESCRIPTION="A small tool for X11 that writes the color value on your screen at the cursor position to stdout, in RGB."
HOMEPAGE="https://github.com/Ancurio/colorpicker"
SRC_URI=""
EGIT_REPO_URI="${HOMEPAGE}.git"
EGIT_BRANCH="master"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~alpha ~amd64 ~arm ~hppa ~ia64 ~ppc ~ppc64 ~s390 ~sh ~sparc ~x86"
IUSE="X"

RDEPEND="
		"x11-libs/gtk+"
"
DEPEND="${RDEPEND}
"

src_compile() {
	make
}

src_install() {
	dobin colorpicker
}

#S=${WORKDIR}/${P}-license

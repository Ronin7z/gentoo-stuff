EAPI=6

inherit git-r3

DESCRIPTION="Graphical application for generating different color variations
of Numix and Materia (ex-Flat-Plat) themes (GTK2, GTK3),
gnome-colors and ArchDroid icon themes.
Have a hack for HiDPI in gtk2."
HOMEPAGE="https://github.com/actionless/oomox"
EGIT_REPO_URI="
	"https://github.com/actionless/oomox.git"
	"https://github.com/actionless/oomox-gtk-theme.git"
	"https://github.com/nana-4/materia-theme.git"
	"https://github.com/actionless/oomox-archdroid-icon-theme.git"
	"https://github.com/actionless/oomox-gnome-colors-icon-theme.git"
	"https://github.com/actionless/oomoxify.git"
"
EGIT_BRANCH="master"

LICENSE="GPL"
SLOT="0"
KEYWORDS="~alpha ~amd64 ~arm ~hppa ~ia64 ~ppc ~ppc64 ~s390 ~sh ~sparc ~x86"

RDEPEND="
"
DEPEND="${RDEPEND}
"

src_configure() {
	git submodule init
	git submodule update
}

src_install() {
	./packaging/install.sh ./ "${ED}"
	python -O -m compileall "${ED}/opt/oomox/oomox_gui"
}

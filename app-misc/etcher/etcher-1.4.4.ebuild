EAPI="5"

#inherit font

DESCRIPTION="Flash OS images to SD cards & USB drives, safely and easily."
HOMEPAGE="https://github.com/resin-io/etcher"


#"https://github.com/resin-io/${PN}/releases/download/v${PV}/${PN}-electron-${PV}-x86_64.zip"

SRC_URI="
	"https://github.com/resin-io/etcher/releases/download/v1.4.4/etcher-electron-1.4.4-x86_64.AppImage"
"

LICENSE="apache"
SLOT="0"
KEYWORDS="~amd64"

S="${WORKDIR}"

src_install() {
        exeinto /usr/share/appimages/${PN}

		cd ../distdir

		doexe "${PN}-electron-${PV}-x86_64.AppImage"

		insinto /usr/bin
		cat << EOF > "${PN}"
#!/bin/bash
exec /usr/share/appimages/${PN}/${PN}-electron-${PV}-x86_64.AppImage $@
EOF
		dobin "${PN}"
}

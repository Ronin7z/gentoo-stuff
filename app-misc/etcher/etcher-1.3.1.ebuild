EAPI="5"

#inherit font

DESCRIPTION="Flash OS images to SD cards & USB drives, safely and easily."
HOMEPAGE="https://github.com/resin-io/etcher"
SRC_URI="
	"https://github.com/resin-io/${PN}/releases/download/v${PV}/${PN}-${PV}-linux-x86_64.zip"
"

LICENSE="apache"
SLOT="0"
KEYWORDS="~amd64"

S="${WORKDIR}"

src_unpack() {
		#cp "${DISTDIR}"/*.ttf "${WORKDIR}"
		for f in "${DISTDIR}"/*.zip
		do
			7z x "$f" -o"${WORKDIR}"
		done
}

src_install() {
        exeinto /usr/share/appimages/${PN}

        cd "${WORKDIR}"

		doexe "${PN}-${PV}-x86_64.AppImage"

		insinto /usr/bin
		cat << EOF > "${PN}"
#!/bin/bash
exec /usr/share/appimages/${PN}/${PN}-${PV}-x86_64.AppImage $@
EOF
		dobin "${PN}"
}

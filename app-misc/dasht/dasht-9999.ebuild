EAPI=6

inherit git-r3

DESCRIPTION="DuckDuckGo from the terminal"
HOMEPAGE="https://github.com/sunaku/dasht"
EGIT_REPO_URI="https://github.com/sunaku/dasht"
EGIT_BRANCH="master"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~alpha ~amd64 ~arm ~hppa ~ia64 ~ppc ~ppc64 ~s390 ~sh ~sparc ~x86"

RDEPEND="
		dev-db/sqlite
"
#
#
#    wget to download docsets from Dash
#
#    w3m to display dasht(1) search results
#
#    socat for dasht-server(1) search engine
#
#    gawk for dasht-server(1) search engine
#
DEPEND="${RDEPEND}
"

#S=${WORKDIR}/ddgr-1.2

src_install() {
  dobin bin/*
  doman man/man1/*

  dodoc README.md LICENSE VERSION.md
}

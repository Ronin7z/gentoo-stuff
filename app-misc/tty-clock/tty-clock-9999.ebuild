EAPI=6

inherit git-r3

DESCRIPTION="tty-clock"
HOMEPAGE="https://github.com/xorg62/tty-clock"
EGIT_REPO_URI="https://github.com/xorg62/tty-clock"
EGIT_BRANCH="master"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~alpha ~amd64 ~arm ~hppa ~ia64 ~ppc ~ppc64 ~s390 ~sh ~sparc ~x86"

RDEPEND=""
DEPEND="${RDEPEND}"

#S="${WORKDIR}"

src_install() {
  dobin tty-clock
  doman "${PN}.1"

  dodoc README
}

EAPI=6

DESCRIPTION="A dmenu frontend to pass with clipboard and autotype functionality for user and password."
HOMEPAGE="https://github.com/klaasb/passdmenu.git"
SRC_URI="https://github.com/klaasb/passdmenu/archive/v1.2.0.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~alpha ~amd64 ~arm ~hppa ~ia64 ~ppc ~ppc64 ~s390 ~sh ~sparc ~x86"

RDEPEND="
		app-admin/pass
"
DEPEND="${RDEPEND}"

#S=${WORKDIR}/passdmenu-1.2.0

src_install() {
  mv passdmenu.py passdmenu
  dobin passdmenu
  dodoc README.md LICENSE.txt
}

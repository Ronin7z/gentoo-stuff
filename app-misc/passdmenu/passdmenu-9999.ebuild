EAPI=6

inherit git-r3

DESCRIPTION="A dmenu frontend to pass with clipboard and autotype functionality for user and password."
HOMEPAGE="https://github.com/klaasb/passdmenu.git"
EGIT_REPO_URI="https://github.com/klaasb/passdmenu.git"
EGIT_BRANCH="master"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~alpha ~amd64 ~arm ~hppa ~ia64 ~ppc ~ppc64 ~s390 ~sh ~sparc ~x86"

RDEPEND="
		app-admin/pass
"
DEPEND="${RDEPEND}"

src_install() {
  mv passdmenu.py passdmenu
  dobin passdmenu
  dodoc README.md LICENSE.txt
}

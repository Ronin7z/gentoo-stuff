EAPI=6

PYTHON_COMPAT=( python3_{4,5,6} pypy{,3} )

inherit distutils-r1

DESCRIPTION="DuckDuckGo from the terminal"
HOMEPAGE="https://github.com/jarun/ddgr"
SRC_URI="https://github.com/jarun/ddgr/archive/v1.2.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~alpha ~amd64 ~arm ~hppa ~ia64 ~ppc ~ppc64 ~s390 ~sh ~sparc ~x86"

RDEPEND="
		dev-python/requests
"
DEPEND="${RDEPEND}
	dev-python/setuptools[${PYTHON_USEDEP}]
"

S=${WORKDIR}/ddgr-1.2

src_compile() {
  emake
}

src_install() {
  emake PREFIX="/usr" DESTDIR="${D}" install

  dodoc README.md LICENSE CHANGELOG
}

EAPI=6

inherit git-r3

DESCRIPTION="Screenshot wrapper for scrot and maim"
HOMEPAGE="https://github.com/Sweets/nougat"
EGIT_REPO_URI="${HOMEPAGE}.git"
EGIT_BRANCH="master"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~alpha ~amd64 ~arm ~hppa ~ia64 ~ppc ~ppc64 ~s390 ~sh ~sparc ~x86"

RDEPEND="
		app-shells/bash
"
DEPEND="${RDEPEND}"

src_install() {
  mv nougat.sh nougat
  dobin nougat

  doman nougat.1

  dodoc README.md LICENSE
}

# Gentoo Stuff

This is my local Gentoo repo that may or may not be maintained and kept up to date.

It contains third-party software I use and any patched ebuilds I'm using (for example to fix LibreSSL related errors)


<!-- ::BEGIN_TREE:: -->
├──
[app-admin](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-admin/)  
│   ├──
[qtpass](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-admin/qtpass/)  
│   └──
[xkcdpass](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-admin/xkcdpass/)  
├──
[app-arch](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-arch/)  
│   └──
[dtrx](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-arch/dtrx/)  
├──
[app-crypt](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-crypt/)  
│   └──
[mit-krb5](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-crypt/mit-krb5/)  
├──
[app-doc](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-doc/)  
│   └──
[zeal](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-doc/zeal/)  
├──
[app-editors](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-editors/)  
│   ├──
[code](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-editors/code/)  
│   ├──
[vim](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-editors/vim/)  
│   ├──
[vim-core](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-editors/vim-core/)  
│   └──
[visual-studio-code](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-editors/visual-studio-code/)  
├──
[app-emulation](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-emulation/)  
│   └──
[vagrant](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-emulation/vagrant/)  
├──
[app-misc](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-misc/)  
│   ├──
[bcal](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-misc/bcal/)  
│   ├──
[dasht](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-misc/dasht/)  
│   ├──
[ddgr](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-misc/ddgr/)  
│   ├──
[etcher](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-misc/etcher/)  
│   ├──
[nnn](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-misc/nnn/)  
│   ├──
[nougat](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-misc/nougat/)  
│   ├──
[passdmenu](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-misc/passdmenu/)  
│   └──
[tty-clock](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-misc/tty-clock/)  
├──
[app-portage](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-portage/)  
│   └──
[eclass-manpages](https://gitlab.com/ahrs/gentoo-stuff/tree/master/app-portage/eclass-manpages/)  
├──
[dev-python](https://gitlab.com/ahrs/gentoo-stuff/tree/master/dev-python/)  
│   ├──
[neovim-python-client](https://gitlab.com/ahrs/gentoo-stuff/tree/master/dev-python/neovim-python-client/)  
│   ├──
[python-mpd](https://gitlab.com/ahrs/gentoo-stuff/tree/master/dev-python/python-mpd/)  
│   └──
[scfbuild](https://gitlab.com/ahrs/gentoo-stuff/tree/master/dev-python/scfbuild/)  
├── [dev-qt](https://gitlab.com/ahrs/gentoo-stuff/tree/master/dev-qt/)  
│   ├──
[qtcore](https://gitlab.com/ahrs/gentoo-stuff/tree/master/dev-qt/qtcore/)  
│   └──
[qtnetwork](https://gitlab.com/ahrs/gentoo-stuff/tree/master/dev-qt/qtnetwork/)  
├──
[dev-util](https://gitlab.com/ahrs/gentoo-stuff/tree/master/dev-util/)  
│   ├──
[jenkins-bin](https://gitlab.com/ahrs/gentoo-stuff/tree/master/dev-util/jenkins-bin/)  
│   ├──
[lxqt-build-tools](https://gitlab.com/ahrs/gentoo-stuff/tree/master/dev-util/lxqt-build-tools/)  
│   └──
[svgo](https://gitlab.com/ahrs/gentoo-stuff/tree/master/dev-util/svgo/)  
├──
[games-util](https://gitlab.com/ahrs/gentoo-stuff/tree/master/games-util/)  
│   └──
[lutris](https://gitlab.com/ahrs/gentoo-stuff/tree/master/games-util/lutris/)  
├──
[media-fonts](https://gitlab.com/ahrs/gentoo-stuff/tree/master/media-fonts/)  
│   ├──
[emojione-color-font](https://gitlab.com/ahrs/gentoo-stuff/tree/master/media-fonts/emojione-color-font/)  
│   ├──
[firacode](https://gitlab.com/ahrs/gentoo-stuff/tree/master/media-fonts/firacode/)  
│   ├──
[nerd-fonts](https://gitlab.com/ahrs/gentoo-stuff/tree/master/media-fonts/nerd-fonts/)  
│   ├──
[nerd-fonts-scripts](https://gitlab.com/ahrs/gentoo-stuff/tree/master/media-fonts/nerd-fonts-scripts/)  
│   └──
[scientifica](https://gitlab.com/ahrs/gentoo-stuff/tree/master/media-fonts/scientifica/)  
├──
[media-libs](https://gitlab.com/ahrs/gentoo-stuff/tree/master/media-libs/)  
│   ├──
[glfw](https://gitlab.com/ahrs/gentoo-stuff/tree/master/media-libs/glfw/)  
│   └──
[opencv](https://gitlab.com/ahrs/gentoo-stuff/tree/master/media-libs/opencv/)  
├──
[media-sound](https://gitlab.com/ahrs/gentoo-stuff/tree/master/media-sound/)  
│   ├──
[cava](https://gitlab.com/ahrs/gentoo-stuff/tree/master/media-sound/cava/)  
│   ├──
[cmus](https://gitlab.com/ahrs/gentoo-stuff/tree/master/media-sound/cmus/)  
│   ├──
[glava](https://gitlab.com/ahrs/gentoo-stuff/tree/master/media-sound/glava/)  
│   ├──
[mpc](https://gitlab.com/ahrs/gentoo-stuff/tree/master/media-sound/mpc/)  
│   ├──
[mpd](https://gitlab.com/ahrs/gentoo-stuff/tree/master/media-sound/mpd/)  
│   ├──
[mpdris2](https://gitlab.com/ahrs/gentoo-stuff/tree/master/media-sound/mpdris2/)  
│   ├──
[pavucontrol-qt](https://gitlab.com/ahrs/gentoo-stuff/tree/master/media-sound/pavucontrol-qt/)  
│   └──
[pulsemixer](https://gitlab.com/ahrs/gentoo-stuff/tree/master/media-sound/pulsemixer/)  
├──
[media-video](https://gitlab.com/ahrs/gentoo-stuff/tree/master/media-video/)  
│   └──
[filebot](https://gitlab.com/ahrs/gentoo-stuff/tree/master/media-video/filebot/)  
├──
[net-dns](https://gitlab.com/ahrs/gentoo-stuff/tree/master/net-dns/)  
│   └──
[bind-tools](https://gitlab.com/ahrs/gentoo-stuff/tree/master/net-dns/bind-tools/)  
├── [net-im](https://gitlab.com/ahrs/gentoo-stuff/tree/master/net-im/)  
│   └──
[beautiful-discord](https://gitlab.com/ahrs/gentoo-stuff/tree/master/net-im/beautiful-discord/)  
├──
[net-libs](https://gitlab.com/ahrs/gentoo-stuff/tree/master/net-libs/)  
│   └──
[nodejs](https://gitlab.com/ahrs/gentoo-stuff/tree/master/net-libs/nodejs/)  
├──
[net-misc](https://gitlab.com/ahrs/gentoo-stuff/tree/master/net-misc/)  
│   └──
[remmina](https://gitlab.com/ahrs/gentoo-stuff/tree/master/net-misc/remmina/)  
├──
[net-p2p](https://gitlab.com/ahrs/gentoo-stuff/tree/master/net-p2p/)  
│   └──
[jackett-bin](https://gitlab.com/ahrs/gentoo-stuff/tree/master/net-p2p/jackett-bin/)  
├──
[sys-apps](https://gitlab.com/ahrs/gentoo-stuff/tree/master/sys-apps/)  
│   ├──
[exa](https://gitlab.com/ahrs/gentoo-stuff/tree/master/sys-apps/exa/)  
│   ├──
[firejail](https://gitlab.com/ahrs/gentoo-stuff/tree/master/sys-apps/firejail/)  
│   ├──
[firetools](https://gitlab.com/ahrs/gentoo-stuff/tree/master/sys-apps/firetools/)  
│   └──
[pacman](https://gitlab.com/ahrs/gentoo-stuff/tree/master/sys-apps/pacman/)  
├── [sys-fs](https://gitlab.com/ahrs/gentoo-stuff/tree/master/sys-fs/)  
│   └──
[dutree](https://gitlab.com/ahrs/gentoo-stuff/tree/master/sys-fs/dutree/)  
├──
[www-misc](https://gitlab.com/ahrs/gentoo-stuff/tree/master/www-misc/)  
│   └──
[buku](https://gitlab.com/ahrs/gentoo-stuff/tree/master/www-misc/buku/)  
├──
[x11-misc](https://gitlab.com/ahrs/gentoo-stuff/tree/master/x11-misc/)  
│   ├──
[brightnessctl](https://gitlab.com/ahrs/gentoo-stuff/tree/master/x11-misc/brightnessctl/)  
│   ├──
[colorpicker](https://gitlab.com/ahrs/gentoo-stuff/tree/master/x11-misc/colorpicker/)  
│   ├──
[dunst](https://gitlab.com/ahrs/gentoo-stuff/tree/master/x11-misc/dunst/)  
│   ├──
[lemonbar](https://gitlab.com/ahrs/gentoo-stuff/tree/master/x11-misc/lemonbar/)  
│   ├──
[polybar](https://gitlab.com/ahrs/gentoo-stuff/tree/master/x11-misc/polybar/)  
│   ├──
[pywal](https://gitlab.com/ahrs/gentoo-stuff/tree/master/x11-misc/pywal/)  
│   ├──
[rofi-top](https://gitlab.com/ahrs/gentoo-stuff/tree/master/x11-misc/rofi-top/)  
│   ├──
[wal\_steam](https://gitlab.com/ahrs/gentoo-stuff/tree/master/x11-misc/wal_steam/)  
│   └──
[wmutils](https://gitlab.com/ahrs/gentoo-stuff/tree/master/x11-misc/wmutils/)  
├──
[x11-terms](https://gitlab.com/ahrs/gentoo-stuff/tree/master/x11-terms/)  
│   └──
[alacritty](https://gitlab.com/ahrs/gentoo-stuff/tree/master/x11-terms/alacritty/)  
├──
[x11-themes](https://gitlab.com/ahrs/gentoo-stuff/tree/master/x11-themes/)  
│   ├──
[adapta-gtk-theme](https://gitlab.com/ahrs/gentoo-stuff/tree/master/x11-themes/adapta-gtk-theme/)  
│   ├──
[adapta-gtk-theme-wal](https://gitlab.com/ahrs/gentoo-stuff/tree/master/x11-themes/adapta-gtk-theme-wal/)  
│   ├──
[oomox](https://gitlab.com/ahrs/gentoo-stuff/tree/master/x11-themes/oomox/)  
│   └──
[SierraBreeze](https://gitlab.com/ahrs/gentoo-stuff/tree/master/x11-themes/SierraBreeze/)  
└── [x11-wm](https://gitlab.com/ahrs/gentoo-stuff/tree/master/x11-wm/)  
    ├──
[custard](https://gitlab.com/ahrs/gentoo-stuff/tree/master/x11-wm/custard/)  
    ├──
[i3-gaps](https://gitlab.com/ahrs/gentoo-stuff/tree/master/x11-wm/i3-gaps/)  
    └──
[xpra](https://gitlab.com/ahrs/gentoo-stuff/tree/master/x11-wm/xpra/)  
  
  

  
  

------------------------------------------------------------------------

tree v1.7.0 © 1996 - 2014 by Steve Baker and Thomas Moore  
HTML output hacked and copyleft © 1998 by Francesc Rocher  
JSON output hacked and copyleft © 2014 by Florian Sesser  
Charsets / OS/2 support © 2001 by Kyosuke Tokoro
